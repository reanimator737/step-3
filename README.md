В команду входят : Каирцев Александр , Вовчук Юрий , Юхимец Антон.
Из-за личных обстоятельств у Антона не вышло нормально участвовать в проекте (о чем мы узнали поздновать)).
Кто что делал:
1)Вовчук Юрий - html верстка(без стилей), фильтр, логику для выбора врача.
2)Каирцев Александр - все остальное.
Использовался только ванильный js

Передалано:
1) Добавил возможность закрыть модалку
2)Юра доработал стили
3)Добавил обработку ошибок (есть только на renderData, остальные по аналогии, очень хочется уже заняться реактом))
4)Немного подкорректировал работу с куки
5)Исправил странные проверки
6)Убрал флаги которые были не нужны
7)Исправил баг с надписью Empty ( не пропадала если карточек на экране нет, а мы добавляем новую)
8)Убрал повторяющийся код в рендере 
9)Оптимизировал проверку времени