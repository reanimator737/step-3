import{VisitCardiologist, VisitDentist, VisitTherapist} from './Visit.js'
import {cookies} from  './work-with-cookies.js'
export async function renderData() {
    try{
        let token = cookies('token');
        let ajax = await  fetch('http://cards.danit.com.ua/cards',{
            headers: {
                Authorization: `Bearer ${token}`
            },
            method: 'GET'
        });
        if(ajax.ok === false){
            throw `Ошибка : ${ajax.status} (${ajax.statusText})`
        }
        let array = await  ajax.json();
        let container = document.querySelector('.posts-container');
        if (array.length > 0){
            document.querySelector('.empty').classList.remove('active');
            container.classList.remove('empty-box');
            array.forEach(elem => {
                switch (elem.doctor) {
                    case "Cardiologist": {
                        new VisitCardiologist(elem);
                        break
                    }
                    case "Dentist":{
                        new VisitDentist(elem);
                        break
                    }
                    case "Therapist":{
                        new VisitTherapist(elem);
                        break
                    }
                }
            })
        } else {
            container.classList.add('empty-box');
        }
    }
    catch (e) {
        console.log(e);
        document.querySelector('.posts-container').classList.add('empty-box');
        alert('Smth go wrong');
    }
}