import {Form, FormRegister} from "./forms.js";
import {renderData} from  './renderData.js'
import {SearchFilter} from './filter.js'
export default class Modal{
    constructor() {
        // Это кнопка вход в аккаунт и создать новое поле
        this.btnStart = document.querySelector('.btn-start');
        if(document.cookie.includes('token')){
            this.btnStart.innerText = 'New post';
            renderData();
            this.addEvent();
            new SearchFilter();
        } else{
            this.addEvent();
        }
    }
    addEvent() {
        this.btnStart.onclick = event => {
            if (event.target.innerText === 'ENTER'){
                //Форма для регистрации(входа)
                new FormRegister('.enter');
                this.btnStart.classList.remove('btn-active');
            } else {
                new Form('.new-post');
                this.btnStart.classList.remove('btn-active');
            }
        };
    }
}


