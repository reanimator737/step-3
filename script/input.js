export class Input {
    constructor(elem) {
        this.elem = elem;
        this.error();
    }
    error(){
        this.elem.id = 'error';
        this.elem.onfocus = () => {
            this.elem.id = '';
        }
    }
}