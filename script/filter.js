import {VisitCardiologist, VisitDentist, VisitTherapist} from "./Visit.js"
import {cookies} from "./work-with-cookies.js";
export class SearchFilter {
    constructor() {
        this.searchDoc = document.querySelector('.search__doc-select');
        this.searchPriority = document.querySelector('.search__urgency');
        this.searchStatus = document.querySelector('.search__status');
        this.searchInput = document.querySelector('.search__text');
        this.searchBtn = document.querySelector('.search__btn');
        document.querySelector('.search-container').classList.add('active');
        this.containerWithCard = document.querySelector('.posts-container');
        this.filteredCard();
    }
    async ajaxCall(){
        const token = cookies('token');
        let ajax = await  fetch('http://cards.danit.com.ua/cards',{
            headers: {
                Authorization: `Bearer ${token}`
            },
            method: 'GET'
        });
        return  await ajax.json();
    }
    filteredCard(){
        this.searchBtn.addEventListener('click', async (event) =>{
            event.preventDefault();
            let arrayF = [this.searchDoc.value, this.searchPriority.value, this.searchStatus.value, this.searchInput.value];
            let dataArray = await this.ajaxCall();
            let filterData = dataArray.filter(elem =>{
                let confirm = [true];
                if(arrayF[0] !== 'All'){
                    confirm.push(elem.doctor === arrayF[0])
                }
                if(arrayF[1] !== 'All'){
                    confirm.push(elem.priority === arrayF[1])
                }
                if(arrayF[2] !== 'All'){
                    let dataTime = elem.time.split('-');
                    let year = dataTime[0];
                    let month = dataTime[1] - 1;
                    let dayAndTime = dataTime[2].split('T');
                    let day = dayAndTime[0];
                    let [hour, min] = dayAndTime[1].split(':');
                    let time = new Date(year, month, day, hour, min);
                    let now = new Date();
                    let different = time - now;
                    if(arrayF[2] === 'Done'){
                        confirm.push(+different <= 0)
                    } else {
                        confirm.push(+different > 0)
                    }

                }
                if(arrayF[3] !== ''){
                    confirm.push(false);
                    for(let item in elem){
                        if(elem[item].toLowerCase().includes(arrayF[3].trim().toLowerCase())){
                            confirm.pop();
                            confirm.push(true);
                            break
                        }
                    }
                }
                return !confirm.includes(false)
            });
            this.containerWithCard.innerHTML = '<h2 class="empty yellow">Empty</h2>';
            this.containerWithCard.classList.remove('empty-box');
            if (filterData.length === 0){
                this.containerWithCard.classList.add('empty-box');
            }
            filterData.forEach(elem => {
                switch (elem.doctor) {
                    case "Cardiologist": {
                        new VisitCardiologist(elem);
                        break
                    }
                    case "Dentist":{
                        new VisitDentist(elem);
                        break
                    }
                    case "Therapist":{
                        new VisitTherapist(elem);
                        break
                    }
                }
        })
    })
    }
}