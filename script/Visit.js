import {Form} from './forms.js'
import {cookies} from  './work-with-cookies.js'
export class Visit {
    constructor(data, card)     {
        this.data =  data;
        this.container = document.querySelector('.posts-container');
        this.box = card || this.createBox();
        this.container.querySelector('.empty').classList.remove('active');
        this.render(card);
    }
    createBox(){
        let box = document.createElement('div');
        box.classList.add('card-box');
        return box
    }
    findEl(arr){
        //Переписать этот костыль
        return{
            rework: arr.find(item =>{
                return item.classList.contains('rework')
                }),
            remove: arr.find(item =>{
                return item.classList.contains('remove')
            }),
            moreInfo: arr.find(item =>{
                return item.classList.contains('more-info')
            }),
            blockWithInfo: arr.find(item =>{
                return item.classList.contains('additional-information')
            })
        };
    }
    showMoreEvent(btn, dopInfo) {
        btn.addEventListener('click', () =>{
            if(dopInfo.classList.contains('additional-information-active')){
                dopInfo.classList.remove('additional-information-active');
                dopInfo.style.maxHeight = 0;
            } else {
                dopInfo.classList.add('additional-information-active');
                dopInfo.style.maxHeight = dopInfo.scrollHeight + 'px';
            }
        })
    }
    eventRework(btn){
        btn.addEventListener('click', () =>{
            this.timeControl(true);
            this.data.card = this.box;
            new Form('.new-post', this.data);
        });
    }
    eventRemove(btn){
        const token = cookies('token');
        const password = cookies('password');
        btn.addEventListener('click', async event => {
            if(prompt('Are you sure you want to delete the card? Enter password') === password){
                this.box.remove();
                let answer = await fetch(`http://cards.danit.com.ua/cards/${this.data.id}`, {
                    headers: {
                        Authorization: `Bearer ${token}`
                    },
                    method: 'DELETE'
                });
                let container = Array.from(document.querySelector('.posts-container').children);
                if(container.length === 1){
                    document.querySelector('.empty').classList.add('active');
                    document.querySelector('.posts-container').classList.add('empty-box');
                }
            } else {
                alert('Wrong password');
            }
        })
    }
    allEvent(obj){
        this.eventRemove(obj.remove);
        this.showMoreEvent(obj.moreInfo, obj.blockWithInfo);
        this.eventRework(obj.rework);
        this.timeControl();
    }
    timeControl(flag){
        this.box.classList.remove('done');
        let dataTime = this.box.querySelector('.box-time').innerText;
        dataTime = dataTime.slice(7).split('-');
        let year = dataTime[0];
        let month = dataTime[1] - 1;
        let dayAndTime = dataTime[2].split('T');
        let day = dayAndTime[0];
        let [hour, min] = dayAndTime[1].split(':');
        let time = new Date(year, month, day, hour, min);
        let now = new Date();
        if (time <= now){
            this.box.classList.add('done');
        } else {
            let timer = setTimeout(()=> this.timeControl(), time-now);
            if(flag){
                clearTimeout(timer)
            }
        }
    }
    render(card){
        this.box.innerHTML = '';
        this.box.insertAdjacentHTML('beforeend', `
             <button class="remove btn">X</button>
             <button class="rework btn">Rework</button>
            <div class="main">
                <p>Doctor : ${this.data.doctor}</p>
                <p>Full Name : ${this.data['full-name']}</p>
                <p>Priority : ${this.data.priority}</p>
                <p class="box-time">Date : ${this.data.time}</p>
            </div>
            <button class="more-info btn">More info</button>
             <div class="additional-information">
                <p>Description : ${this.data.description}</p>
                <p>Purpose : ${this.data.purpose}</p>
            </div>
            `
        );
        if (!card){
            this.container.prepend(this.box);
        }

        //Добавляем ивенты к кнопкам
        let objWithData = this.findEl(Array.from(this.box.children));
        this.allEvent(objWithData)
    }
}
export class VisitCardiologist extends  Visit{
    constructor(data, flag) {
        super(data, flag);
    }
    render(card){
        super.render(card);
        this.box.querySelector('.additional-information').insertAdjacentHTML('beforeEnd', `
                <p>Normal Pressure : ${this.data['normal-pressure']}</p>
                <p>BMI : ${this.data.BMI}</p>
                <p>Age : ${this.data.age}</p>
                <p>Diseases : ${this.data.diseases}</p>
            `
        );
    }
}
export class VisitDentist extends  Visit{
    constructor(data, flag) {
        super(data, flag);
    }
    render(card){
        super.render(card);
        this.box.querySelector('.additional-information').insertAdjacentHTML('beforeEnd', `<p>Last Visit : ${this.data['last-visit']}</p>`);

    }
}
export class VisitTherapist extends  Visit{
    constructor(data, flag) {
        super(data, flag);

    }
    render(card){
        super.render(card);
        this.box.querySelector('.additional-information').insertAdjacentHTML('beforeEnd', `<p>Age : ${this.data.age}</p>`);
    }
}