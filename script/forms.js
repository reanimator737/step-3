import {VisitDentist, VisitCardiologist, VisitTherapist} from './Visit.js'
import {renderData} from './renderData.js';
import {Input} from './input.js'
import {SearchFilter} from './filter.js'
import {cookies} from  './work-with-cookies.js'
export  class Form{
    constructor(formClass, flag) {
        // flag содержит в себе все данные карточки + саму карточку
        this.selectDoc = document.querySelector('.select');
        this.cardiologistForm = document.querySelector('.new-post__cardiologist');
        this.dentistForm = document.querySelector('.new-post__dentist');
        this.therapistForm = document.querySelector('.new-post__therapist');
        this.form = document.querySelector(formClass);
        this.checkFlag(flag);
        this.reworkSubmit(flag);
        this.eventCheckDoctor();
        this.blur();
    }
    blur(){
        let blur =  document.querySelector('div.modal-bg');
        blur.onclick = (event) =>{
            if(event.target === blur){  
                return this.end()
            }
        };
        blur.classList.add('blur');
        document.querySelector("body").style.overflow = 'hidden';
        blur.style.top =  `${this.getBodyScrollTop()}px`;
    }
    checkFlag(flag) {
        if (flag) {
           this.activeFlagControl(flag);
            this.form.classList.add('active');
            this.blur();
            this.fillInForm(this.form.children, flag);
        } else {
            this.form.classList.add('active');
        }
    }
    activeFlagControl(flag){
        [this.cardiologistForm, this.dentistForm, this.therapistForm].forEach(elem =>{
            if(elem.classList.contains(`new-post__${flag.doctor.toLowerCase()}`)){
                elem.classList.add('active');
            } else {
                elem.classList.remove('active')
            }
        });
    }
    reworkSubmit(flag){
        // Меняем стандартное поведения кнопки submit. Теперь по ее нажатию формируется объект  ключ который можно посмотерть у дочерних классов, а значение === значению в форме
            this.form.onsubmit = (event)=> {
                event.preventDefault();
                this.workWithForm(flag);

            }
    }
    workWithForm(flag) {
        document.querySelector('.posts-container').classList.remove('empty-box');
        let infoForm = this.checkActive();
        let data = {};
        let control;
        let generalInfo = document.querySelector('.new-post__general');
        Array.from(generalInfo.children).forEach(elem => {
           if (elem.value === ''){
               control = true;
               new Input(elem)
           }
           data[elem.name] = elem.value;
        });
        Array.from(infoForm.form.children).forEach(elem => {
            if (elem.value === ''){
                control = true;
                new Input(elem)
            }
            data[elem.name] = elem.value;
        });
        data.doctor = infoForm.doctor;
        if (!control) {
            this.AjaxCall(data, flag).then(content => {
                if (infoForm.doctor === 'Cardiologist') {
                    new VisitCardiologist(content, flag && flag.card)
                } else if (infoForm.doctor === 'Dentist') {
                    new VisitDentist(content, flag && flag.card)
                } else {
                    new VisitTherapist(content, flag && flag.card)
                }
                this.end();
            })
        } else {
            this.reworkSubmit(flag);
        }
    }
    checkActive(){
        if(this.cardiologistForm.classList.contains('active')){
            return {
                doctor: 'Cardiologist',
                form: this.cardiologistForm
            }
        } else if(this.dentistForm.classList.contains('active')){
            return {
                doctor: 'Dentist',
                form: this.dentistForm
            }
        } else {
            return {
                doctor: 'Therapist',
                form: this.therapistForm
            }
        }
    }
    cleanPartOfForm(part){
        Array.from(part.children).forEach(elem =>{
            elem.value = '';
        })
    }
    eventCheckDoctor(){
        this.selectDoc.addEventListener('change', ()=> {
            switch (this.selectDoc.value) {
                case "Cardiologist":
                    this.dentistForm.classList.remove('active');
                    this.therapistForm.classList.remove('active');
                    this.cleanPartOfForm(this.dentistForm);
                    this.cleanPartOfForm(this.therapistForm);
                    this.cardiologistForm.classList.add('active');
                    break;

                case "Dentist":
                    this.cardiologistForm.classList.remove('active');
                    this.therapistForm.classList.remove('active');
                    this.cleanPartOfForm(this.cardiologistForm);
                    this.cleanPartOfForm(this.therapistForm);
                    this.dentistForm.classList.add('active');
                    break;

                case "Therapist":
                    this.dentistForm.classList.remove('active');
                    this.cardiologistForm.classList.remove('active');
                    //this.cleanPartOfForm(this.cardiologistForm);
                    //this.cleanPartOfForm(this.dentistForm);
                    this.therapistForm.classList.add('active');
                    break;

                default: return
            }
        });

    }
    async AjaxCall(obj, flag){
        let token = cookies('token');
        if (flag){
            let id = flag.id;
            let reworkAjax = await fetch(`http://cards.danit.com.ua/cards/${id}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                },
                method: 'PUT',
                body: JSON.stringify(obj)
            });
            return await reworkAjax.json()
        }
        let ajax = await fetch('http://cards.danit.com.ua/cards',{
            headers: {
                Authorization: `Bearer ${token}`
            },
            method: 'POST',
            body: JSON.stringify(obj)
        });
        return await ajax.json()
    }
    fillInForm(collection,obj){
        //Рекурсивно заполняю нужные инпуты в форме
        Array.from(collection).forEach(elem => {
            if(elem.tagName.toLowerCase() === 'fieldset' && (elem.classList.contains(`new-post__${obj.doctor.toLowerCase()}`) || elem.classList.contains('new-post__general')))
            {
                this.fillInForm(elem.children, obj)
            } else if(elem.type.toLowerCase() === 'submit'){
                // Ничего не произойдет, просто переход на следущую итерацию
                return
            } else if (elem.tagName.toLowerCase() !== 'fieldset'){
                elem.value = obj[elem.name];
            }
        })

    }
    end(){
        document.querySelector("body").style.overflow = 'auto';
        this.form.classList.remove('active');
        document.querySelector('.btn-start').classList.add('btn-active');
        this.form.reset();
        this.cardiologistForm.classList.add('active');
        this.dentistForm.classList.remove('active');
        this.therapistForm.classList.remove('active');
        document.querySelector('div.modal-bg').classList.remove('blur');
    }
    getBodyScrollTop() {
        return self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop);
    }
}
export  class FormRegister{
    constructor(formClass) {
        this.form = document.querySelector(formClass);
        this.form.classList.add('active');
        this.reworkSubmit();
        // Тут сработала такая цепочка:  мы вызвали reworkSubmit который изменил default event и через return вызвала нам workWithForm
    }
    reworkSubmit(){
        // Меняем стандартное поведения кнопки submit. Теперь по ее нажатию формируется объект  ключ который можно посмотерть у дочерних классов, а значение === значению в форме
        this.form.addEventListener('submit', event => {
            event.preventDefault();
            this.workWithForm()
        },)
    }
    workWithForm(){
        let data = new FormData(this.form);
        let postData = {
            email: data.get('email'),
            password: data.get('password')
        };
        this.ajaxPOST(postData)
    }
    async ajaxPOST(postData){
        let data = await fetch('http://cards.danit.com.ua/login',{
            method: 'POST',
            body: JSON.stringify(postData)
        });
        let answer = await  data.json();
        if (answer.token){
            document.cookie = `password = ${postData.password}`;
            document.cookie = `token = ${answer.token}`;
            this.end();
            return answer.token
        } else {
            alert('Is there something wrong');
        }
    }
    end() {
        renderData();
        new SearchFilter();
        document.querySelector('.btn-start').classList.add('btn-active');
        this.form.classList.remove('active');
        document.querySelector('.btn-start').innerHTML = 'New post';
    }
}