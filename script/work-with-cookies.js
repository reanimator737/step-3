export function cookies(word) {
    const WORD = word.toLowerCase().trim();
    const data = document.cookie;
    const startIndex = data.indexOf(WORD);
    if (startIndex === -1){
        // На самом деле обработки ошибки тут нет, это фактически флаг для теста/разработки ( ибо тут пользователь ничего сломать не может)
        throw 'There is no such thing in cookies'
    }
    // +1 ибо в куки всегда выглядит ключ=значение и с помощью +1 мы ловим пробел
    const length = WORD.length + 1;
    let endIndex = data.indexOf(';', startIndex);
    if(endIndex === -1) {
        // Удобный абуз спецификации
        endIndex = undefined
    }
    return data.slice(startIndex + length, endIndex).trim();
}